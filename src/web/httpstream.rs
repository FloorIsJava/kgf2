//! Provides a stream for parsing HTTP requests.
use crate::util::stoptoken::StopToken;
use http::request::{Builder, Request};
use http::Version;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io::{self, ErrorKind, Read};
use std::str::{from_utf8, Utf8Error};
use std::thread;

/// A wrapper for a `Read`able object with helpers for parsing HTTP requests.
pub struct HttpStream<'a, S>
where
  S: Read,
{
  /// The underlying stream.
  base: &'a mut S,
  /// The internal buffer of read but unused data.
  buffer: VecDeque<u8>,
  /// Token that controls whether a stop has been requested.
  stop_token: StopToken,
}

/// Indicates errors with a [HttpStream].
#[derive(Debug)]
pub enum StreamError {
  /// Error when an abort has been requested by the user.
  AbortRequested,
  /// Unable to decode parts of the stream.
  Decode(Utf8Error),
  /// The (unexpected) end of the HTTP header has been reached.
  EndOfHeader,
  /// The (unexpected) end of file has been reached.
  Eof,
  /// The format of the request is invalid.
  Format(String),
  /// An error from the [http] crate.
  Http(http::Error),
  /// An error from IO.
  Io(io::Error),
  /// The HTTP version is invalid.
  Version(String),
}

/// A line of the request header.
enum HttpLine {
  /// A regular line.
  Normal(String),
  /// The final empty line.
  Final,
}

/// Used to convert `&str` into `Version`.
trait IntoVersion {
  /// Convert the `Self` into `Version`, if possible.
  fn into_version(self) -> Result<Version, StreamError>;
}

pub type BuilderResult = Result<Builder, StreamError>;
pub type RequestResult = Result<Request<Vec<u8>>, StreamError>;

impl<'a, S> HttpStream<'a, S>
where
  S: Read,
{
  /// Creates a new stream.
  ///
  /// # Arguments
  /// `stream`: The underlying stream.
  /// `stop_token`: A stop token for communicating stopping intent.
  pub fn new(stream: &'a mut S, stop_token: StopToken) -> Self {
    Self { base: stream, buffer: VecDeque::new(), stop_token }
  }

  /// Reads the request line ("GET /foo HTTP/1.2.3").
  ///
  /// # Arguments
  /// `builder`: The builder to be updated.
  pub fn read_request_line(&mut self, builder: Builder) -> BuilderResult {
    let line = match self.read_http_line()? {
      HttpLine::Normal(l) => l,
      HttpLine::Final => return Err(StreamError::EndOfHeader),
    };

    let parts: Vec<&str> = line.split(' ').collect();
    if parts.len() != 3 {
      let msg = format!("request line has {} parts, needs 3", parts.len());
      return Err(StreamError::Format(msg));
    }
    let builder = builder.method(parts[0]);
    let builder = builder.uri(parts[1]);
    Ok(builder.version(parts[2].into_version()?))
  }

  /// Reads the request headers.
  ///
  /// # Arguments
  /// `builder`: The builder to be updated.
  pub fn read_headers(&mut self, mut builder: Builder) -> BuilderResult {
    while let HttpLine::Normal(line) = self.read_http_line()? {
      let parts: Vec<&str> = line.splitn(2, ':').collect();
      if parts.len() != 2 {
        let msg = format!("header line has {} parts, needs 2", parts.len());
        return Err(StreamError::Format(msg));
      }
      let header = parts[0].trim();
      let value = parts[1].trim();
      builder = builder.header(header, value);
    }
    Ok(builder)
  }

  /// Reads the request body.
  ///
  /// # Arguments
  /// `builder`: The builder to be updated.
  pub fn read_body(&mut self, builder: Builder) -> RequestResult {
    // Get the content length
    let content_length =
      match builder.headers_ref().and_then(|x| x.get("content-length")) {
        // Header present, parse it
        Some(v) => match v.to_str().map(|x| x.parse::<usize>()) {
          // Successfully parsed
          Ok(Ok(v)) => v,
          // Parse unsuccessful
          Ok(Err(_)) | Err(_) => {
            let msg = format!("invalid content length '{:?}'", v);
            return Err(StreamError::Format(msg));
          }
        },
        // Header not present
        None => 0,
      };

    let mut v: Vec<u8> = Vec::new();
    if content_length > 0 {
      'line_reader: loop {
        // Read until content length is satisfied
        while let Some(b) = self.buffer.pop_front() {
          v.push(b);
          if v.len() == content_length {
            break 'line_reader;
          }
        }

        // Refill buffer
        self.refill_buffer()?;
      }
    }
    builder.body(v).map_err(Into::into)
  }

  /// Reads a line from a HTTP request header.
  fn read_http_line(&mut self) -> Result<HttpLine, StreamError> {
    let mut line_buf = Vec::new();
    'line_reader: loop {
      // Read until \r\n or until buffer is empty
      while let Some(b) = self.buffer.pop_front() {
        line_buf.push(b);
        if b == b'\n' {
          let len = line_buf.len();
          if len >= 2 && line_buf[len - 2] == b'\r' {
            break 'line_reader;
          }
        }
      }

      // Refill buffer
      self.refill_buffer()?;
    }

    if line_buf.len() == 2 {
      Ok(HttpLine::Final)
    } else {
      Ok(HttpLine::Normal(from_utf8(&line_buf)?.trim().to_string()))
    }
  }

  /// Refills the internal buffer.
  ///
  /// # Returns
  /// `Err(())` if no data could be refilled, `Ok(())` otherwise.
  fn refill_buffer(&mut self) -> Result<(), StreamError> {
    let mut buf = [0; 1024];

    let read_count;
    loop {
      match self.base.read(&mut buf) {
        Ok(x) => {
          read_count = x;
          break;
        }
        Err(ref e)
          if e.kind() == ErrorKind::WouldBlock
            || e.kind() == ErrorKind::TimedOut =>
        {
          if self.stop_token.is_stopped() {
            return Err(StreamError::AbortRequested);
          }
          thread::yield_now();
        }
        Err(e) => {
          return Err(StreamError::Io(e));
        }
      }
    }

    for byte in &buf[..read_count] {
      self.buffer.push_back(*byte);
    }
    if read_count == 0 {
      Err(StreamError::Eof)
    } else {
      Ok(())
    }
  }
}

impl Error for StreamError {
  fn source(&self) -> Option<&(dyn Error + 'static)> {
    match self {
      Self::Decode(e) => Some(e),
      Self::Http(e) => Some(e),
      Self::Io(e) => Some(e),
      _ => None,
    }
  }
}

impl Display for StreamError {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}

impl From<Utf8Error> for StreamError {
  fn from(e: Utf8Error) -> Self {
    Self::Decode(e)
  }
}

impl From<http::Error> for StreamError {
  fn from(e: http::Error) -> Self {
    Self::Http(e)
  }
}

impl From<io::Error> for StreamError {
  fn from(e: io::Error) -> Self {
    Self::Io(e)
  }
}

impl IntoVersion for &str {
  fn into_version(self) -> Result<Version, StreamError> {
    match self {
      "HTTP/0.9" => Ok(Version::HTTP_09),
      "HTTP/1.0" => Ok(Version::HTTP_10),
      "HTTP/1.1" => Ok(Version::HTTP_11),
      "HTTP/2.0" => Ok(Version::HTTP_2),
      "HTTP/3.0" => Ok(Version::HTTP_3),
      _ => Err(StreamError::Version(format!("invalid version '{}'", self))),
    }
  }
}
