//! Provides HTTP parsing functionality.
use super::httpstream::{HttpStream, StreamError};
use crate::util::stoptoken::StopToken;
use http::request::Builder;
use http::Request;
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io::Read;

/// Indicates errors with parsing requests.
#[derive(Debug)]
pub enum RequestError {
  /// An error with basic stream parsing.
  Stream(StreamError),
  /// An error from the [http] crate.
  Http(http::Error),
}

type ReqResult = Result<Request<Vec<u8>>, RequestError>;

/// Provides the next request from the stream, if any.
///
/// # Arguments
/// `stream`: The stream to read from.
/// `stop_token`: A stop token for communicating a stopping intent.
pub fn next_request<S>(
  stream: &mut S,
  stop_token: StopToken,
) -> Option<ReqResult>
where
  S: Read,
{
  let mut stream = HttpStream::new(stream, stop_token);
  match stream
    .read_request_line(Builder::new())
    .and_then(|x| stream.read_headers(x))
    .and_then(|x| stream.read_body(x))
  {
    Ok(v) => Some(Ok(v)),
    Err(StreamError::Eof) => None,
    Err(e) => Some(Err(e.into())),
  }
}

impl From<StreamError> for RequestError {
  fn from(e: StreamError) -> Self {
    RequestError::Stream(e)
  }
}

impl From<http::Error> for RequestError {
  fn from(e: http::Error) -> Self {
    RequestError::Http(e)
  }
}

impl Error for RequestError {
  fn source(&self) -> Option<&(dyn Error + 'static)> {
    match self {
      RequestError::Stream(e) => Some(e),
      RequestError::Http(e) => Some(e),
    }
  }
}

impl Display for RequestError {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}
