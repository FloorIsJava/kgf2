//! Provides a HTTP web server.
use super::httpparse::next_request;
use super::httpwriter::HttpResponse;
use crate::controller::{text_response, Router};
use crate::util::executor::Executor;
use crate::util::stoptoken::StopToken;
use std::io::{ErrorKind, Write};
use std::net::{TcpListener, TcpStream};
use std::thread;
use std::time::Duration;

/// A webserver.
pub struct WebServer {
  /// The join handle for the web server thread.
  join_handle: Option<thread::JoinHandle<()>>,
  /// A stop token for the web server.
  stop_token: StopToken,
}

/// Implementation details of the webserver's thread.
struct WebServerImpl {
  /// A stop token for the web server.
  stop_token: StopToken,
}

impl WebServer {
  /// Creates a new web server.
  pub fn new() -> Self {
    WebServer { join_handle: None, stop_token: StopToken::new() }
  }

  /// Runs the webserver. This call does not block.
  pub fn run(&mut self) {
    let token = self.stop_token.clone();
    self.join_handle = Some(thread::spawn(move || {
      let wimpl = WebServerImpl::new(token);
      wimpl.run();
    }));
  }
}

impl Drop for WebServer {
  fn drop(&mut self) {
    self.stop_token.stop();

    if let Some(handle) = self.join_handle.take() {
      handle.join().unwrap();
    }
  }
}

impl WebServerImpl {
  /// Creates a new implementation object.
  ///
  /// # Arguments
  /// `stop_token`: A token for propagating stopping intentions.
  fn new(stop_token: StopToken) -> Self {
    Self { stop_token }
  }

  /// Runs the logic of the web server.
  fn run(&self) {
    let executor = Executor::new(8);

    let listener = TcpListener::bind("127.0.0.1:8091").unwrap();
    listener.set_nonblocking(true).expect("cannot set non-blocking");
    for stream in listener.incoming() {
      let token = self.stop_token.clone();
      match stream {
        Ok(stream) => executor.execute(move || {
          WebServerImpl::handle_connection(stream, token);
        }),
        Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
          thread::sleep(Duration::from_millis(50));
        }
        Err(e) => panic!("io error: {}", e),
      }

      if self.stop_token.is_stopped() {
        break;
      }
    }
  }

  /// Handles an incoming connection.
  ///
  /// # Arguments
  /// `stream`: The stream of the connection.
  /// `token`: A token for signalling stopping intentions.
  fn handle_connection(mut stream: TcpStream, token: StopToken) {
    stream.set_read_timeout(Some(Duration::from_secs(1))).unwrap();

    let router = Router::new();
    while let Some(req) = next_request(&mut stream, token.clone()) {
      let mut response = match &req {
        Err(_) => text_response(400, "400 Bad Request").unwrap(),
        Ok(v) => router.route(v),
      };

      match response.prepare() {
        Ok(_) => {
          if let Ok(v) = req {
            if let Err(err) = response.post_prepare(v) {
              eprintln!("Error from post_prepare: {}", err);
              WebServerImpl::internal_error(&mut stream);
              break;
            }
          }

          match response.send(&mut stream) {
            Ok(_) => {
              stream.flush().unwrap();
              continue;
            }
            Err(err) => {
              eprintln!("Error from send: {}", err);
              break;
            }
          }
        }
        Err(err) => {
          eprintln!("Error from prepare: {}", err);
        }
      }

      WebServerImpl::internal_error(&mut stream);
      break;
    }
  }

  /// Delivers an internal error to the given stream.
  ///
  /// # Arguments
  /// `stream`: The stream.
  fn internal_error(stream: &mut TcpStream) {
    let mut response = text_response(500, "500 Internal Server Error").unwrap();
    response.prepare().unwrap();
    if let Err(_) = response.send(stream) {
      return;
    }

    if let Err(_) = stream.flush() {
      return;
    }
  }
}
