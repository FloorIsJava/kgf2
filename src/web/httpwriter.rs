//! Provides functionality for writing HTTP responses.
use flate2::write::GzEncoder;
use flate2::Compression;
use http::header::{
  HeaderValue, ACCEPT_ENCODING, CONTENT_ENCODING, CONTENT_LENGTH, CONTENT_TYPE,
};
use http::{Request, Response, Version};
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::io::Write;
use std::mem::swap;

/// HTTP responses which can be sent to clients.
pub trait HttpResponse {
  /// Prepares the response to be sent.
  fn prepare(&mut self) -> Result<(), ResponseError>;

  /// Performs request-sensitive post-processing.
  ///
  /// # Arguments
  /// `req`: The request.
  fn post_prepare(
    &mut self,
    req: Request<Vec<u8>>,
  ) -> Result<(), ResponseError>;

  /// Sends the response to the given stream.
  ///
  /// # Arguments
  /// `stream`: The stream to send the response to.
  fn send<S>(&self, stream: &mut S) -> Result<(), ResponseError>
  where
    S: Write;
}

/// Errors from sending responses.
#[derive(Debug)]
pub enum ResponseError {
  /// Io-based errors.
  Io(std::io::Error),
  /// Http crate based errors.
  Http(http::Error),
  /// An invalid HTTP version.
  InvalidVersion,
  /// An invalid HTTP status.
  InvalidStatus,
}

/// Versions convertible to `str`.
trait FromVersion {
  /// Converts the `Version` to `str`, if possible.
  fn from_version(self) -> Option<&'static str>;
}

impl HttpResponse for Response<Vec<u8>> {
  fn prepare(&mut self) -> Result<(), ResponseError> {
    // Infer a content type if none is set
    if let None = self.headers().get(CONTENT_TYPE) {
      if let Some(mime) = infer::get(&self.body()) {
        let typ = HeaderValue::from_str(mime.mime_type());
        let typ = typ.map_err(http::Error::from);
        self.headers_mut().insert(CONTENT_TYPE, typ?);
      }
    }

    Ok(())
  }

  fn post_prepare(
    &mut self,
    req: Request<Vec<u8>>,
  ) -> Result<(), ResponseError> {
    // Apply gzip-compression if supported by the client
    if let Some(enc) = req.headers().get(ACCEPT_ENCODING) {
      if let Ok(modes) = enc.to_str() {
        let modes: Vec<&str> = modes.split(',').map(str::trim).collect();
        if modes.contains(&"gzip") {
          let enc = HeaderValue::from_str("gzip");
          let enc = enc.map_err(http::Error::from);
          self.headers_mut().insert(CONTENT_ENCODING, enc?);

          let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
          encoder.write_all(&self.body()).unwrap();
          swap(&mut encoder.finish().unwrap(), self.body_mut());
        }
      }
    }

    // Insert the correct content length
    let len = HeaderValue::from_str(&self.body().len().to_string());
    let len = len.map_err(http::Error::from);
    self.headers_mut().insert(CONTENT_LENGTH, len?);

    Ok(())
  }

  fn send<S>(&self, stream: &mut S) -> Result<(), ResponseError>
  where
    S: Write,
  {
    write!(
      stream,
      "{} {} {}\r\n",
      self.version().from_version().ok_or(ResponseError::InvalidVersion)?,
      self.status().as_str(),
      self.status().canonical_reason().ok_or(ResponseError::InvalidStatus)?
    )?;
    for (name, val) in self.headers() {
      write!(stream, "{}: ", name)?;
      stream.write(val.as_bytes())?;
      write!(stream, "\r\n")?;
    }
    write!(stream, "\r\n")?;
    stream.write(&self.body())?;
    Ok(())
  }
}

impl From<std::io::Error> for ResponseError {
  fn from(err: std::io::Error) -> Self {
    ResponseError::Io(err)
  }
}

impl From<http::Error> for ResponseError {
  fn from(err: http::Error) -> Self {
    ResponseError::Http(err)
  }
}

impl Display for ResponseError {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}

impl Error for ResponseError {
  fn source(&self) -> Option<&(dyn Error + 'static)> {
    match self {
      ResponseError::Io(e) => Some(e),
      ResponseError::Http(e) => Some(e),
      _ => None,
    }
  }
}

impl FromVersion for Version {
  fn from_version(self) -> Option<&'static str> {
    match self {
      Version::HTTP_09 => Some("HTTP/0.9"),
      Version::HTTP_10 => Some("HTTP/1.0"),
      Version::HTTP_11 => Some("HTTP/1.1"),
      Version::HTTP_2 => Some("HTTP/2.0"),
      Version::HTTP_3 => Some("HTTP/3.0"),
      _ => None,
    }
  }
}
