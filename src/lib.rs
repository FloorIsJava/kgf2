pub mod application;
pub mod controller;
pub mod util;
pub mod web;

use application::Application;
use std::panic::{set_hook, take_hook};
use std::process::exit;

/// Runs the application.
pub fn run_app() {
  let hook = take_hook();
  set_hook(Box::new(move |panic_info| {
    hook(panic_info);
    exit(1);
  }));

  let mut app = Application::new();
  app.run();
}
