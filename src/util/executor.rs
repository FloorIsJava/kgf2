//! Provides an executor for multithreaded task dispatching.
use std::sync::{mpsc, Arc, Mutex};
use std::thread;

/// An executor which dispatchs tasks into a thread pool.
pub struct Executor {
  /// The workers of the executor.
  workers: Vec<Worker>,
  /// The sending end of the control message channel.
  sender: mpsc::Sender<Message>,
}

/// Indistinguishable workers of an executor.
struct Worker {
  /// The thread that is managed by this worker.
  handle: ThreadHandle,
}

/// Messages that can be sent to workers of an executor.
enum Message {
  /// This message instructs the worker to terminate itself.
  Terminate,
  /// This message instructs the worker to execute the given job.
  Task(Job),
}

type Job = Box<dyn FnOnce() + Send + 'static>;
type SharedReceiver = Arc<Mutex<mpsc::Receiver<Message>>>;
type ThreadHandle = Option<thread::JoinHandle<()>>;

impl Executor {
  /// Creates an executor.
  ///
  /// # Arguments
  /// `size`: The number of threads in the executor.
  ///
  /// # Returns
  /// The newly created executor.
  ///
  /// # Panics
  /// If `size == 0`.
  pub fn new(size: usize) -> Self {
    assert!(size > 0);

    let (sender, receiver) = mpsc::channel();
    let receiver = Arc::new(Mutex::new(receiver));

    let mut workers = Vec::with_capacity(size);
    for _ in 0..size {
      workers.push(Worker::new(Arc::clone(&receiver)));
    }
    Executor { workers, sender }
  }

  /// Executes a task in a thread of this executor.
  ///
  /// # Arguments
  /// `f`: The task to execute.
  ///
  /// # Examples
  /// ```
  /// use kgf2::util::executor::Executor;
  /// use kgf2::util::stoptoken::StopToken;
  ///
  /// let token = StopToken::new();
  /// let exec_tkn = token.clone();
  ///
  /// let executor = Executor::new(1);
  /// executor.execute(move || exec_tkn.stop());
  ///
  /// drop(executor);
  /// assert!(token.is_stopped());
  /// ```
  pub fn execute<F>(&self, f: F)
  where
    F: FnOnce() + Send + 'static,
  {
    let msg = Message::Task(Box::new(f));
    self.sender.send(msg).unwrap();
  }
}

impl Drop for Executor {
  fn drop(&mut self) {
    for _ in &self.workers {
      self.sender.send(Message::Terminate).unwrap();
    }

    for worker in &mut self.workers {
      if let Some(thread) = worker.handle.take() {
        thread.join().unwrap();
      }
    }
  }
}

impl Worker {
  /// Creates a worker.
  ///
  /// # Arguments
  /// `receiver`: The receiving end of the control message channel.
  fn new(receiver: SharedReceiver) -> Self {
    let thread = thread::spawn(move || loop {
      let msg = receiver.lock().unwrap().recv().unwrap();
      match msg {
        Message::Terminate => return,
        Message::Task(j) => j(),
      }
    });
    Worker { handle: Some(thread) }
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn test_execute_all() {
    let exec = Executor::new(8);

    let ctr: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
    for i in 1..=100 {
      let ctr_access = ctr.clone();
      exec.execute(move || *ctr_access.lock().unwrap() += i);
    }

    drop(exec);
    assert_eq!(5050, *ctr.lock().unwrap());
  }
}
