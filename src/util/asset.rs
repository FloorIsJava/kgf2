//! Handling of static assets.
use rust_embed::RustEmbed;

/// Embedded application assets.
///
/// # Examples
/// ```
/// use kgf2::util::asset::Asset;
/// assert!(Asset::get("app/tpl/index.html").is_some());
/// ```
/// The `app/` prefix is required.
/// ```
/// use kgf2::util::asset::Asset;
/// assert!(Asset::get("tpl/index.html").is_none());
/// ```
#[derive(RustEmbed)]
#[folder = "app/"]
#[prefix = "app/"]
pub struct Asset;
