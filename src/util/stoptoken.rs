//! Provides a token for cooperative stopping.
use std::ops::DerefMut;
use std::sync::{Arc, Mutex};

/// A token for communicating stop intentions.
///
/// # Examples
/// ```
/// use kgf2::util::stoptoken::StopToken;
/// let token = StopToken::new();
/// token.stop();
/// assert!(token.is_stopped());
/// ```
pub struct StopToken {
  /// The internal implementation of the token.
  token: Arc<Mutex<bool>>,
}

impl StopToken {
  /// Creates a new stop token.
  ///
  /// # Returns
  /// The newly created token.
  ///
  /// # Examples
  /// ```
  /// use kgf2::util::stoptoken::StopToken;
  /// let token = StopToken::new();
  /// assert!(!token.is_stopped());
  /// ```
  pub fn new() -> Self {
    Self { token: Arc::new(Mutex::new(false)) }
  }

  /// Signals a stop intention to the token.
  ///
  /// This operation is idempotent.
  ///
  /// # Examples
  /// ```
  /// use kgf2::util::stoptoken::StopToken;
  /// let token = StopToken::new();
  /// token.stop();
  /// assert!(token.is_stopped());
  /// ```
  /// The operation is idempotent.
  /// ```
  /// use kgf2::util::stoptoken::StopToken;
  /// let token = StopToken::new();
  /// token.stop();
  /// assert!(token.is_stopped());
  /// token.stop();
  /// assert!(token.is_stopped());
  /// ```
  pub fn stop(&self) {
    *self.token.lock().unwrap().deref_mut() = true;
  }

  /// Checks whether a stop intention has been signalled.
  ///
  /// # Returns
  /// `true` if a stop intention has been signalled.
  ///
  /// # Examples
  /// ```
  /// use kgf2::util::stoptoken::StopToken;
  /// let token = StopToken::new();
  /// assert!(!token.is_stopped());
  /// token.stop();
  /// assert!(token.is_stopped());
  /// ```
  pub fn is_stopped(&self) -> bool {
    *self.token.lock().unwrap()
  }
}

impl Clone for StopToken {
  /// Obtains a token sharing the same state.
  ///
  /// # Returns
  /// The cloned token.
  ///
  /// # Examples
  /// ```
  /// use kgf2::util::stoptoken::StopToken;
  ///
  /// let token = StopToken::new();
  /// let tok2 = token.clone();
  ///
  /// assert!(!token.is_stopped());
  /// assert!(!tok2.is_stopped());
  /// tok2.stop();
  /// assert!(token.is_stopped());
  /// assert!(tok2.is_stopped());
  ///
  /// let tok3 = tok2.clone();
  /// assert!(token.is_stopped());
  /// assert!(tok2.is_stopped());
  /// assert!(tok3.is_stopped());
  /// ```
  ///
  /// ```
  /// use std::thread;
  /// use kgf2::util::stoptoken::StopToken;
  ///
  /// let token = StopToken::new();
  ///
  /// let tok2 = token.clone();
  /// let handle = thread::spawn(move || tok2.stop());
  /// handle.join().unwrap();
  ///
  /// assert!(token.is_stopped());
  /// ```
  fn clone(&self) -> Self {
    Self { token: self.token.clone() }
  }
}
