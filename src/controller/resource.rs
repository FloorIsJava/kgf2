//! Controller that delivers resources.
use super::{text_response, Controller};
use crate::util::asset::Asset;
use crc32fast::Hasher;
use http::header::{
  HeaderValue, CACHE_CONTROL, CONTENT_TYPE, ETAG, IF_NONE_MATCH,
};
use http::{Request, Response, Version};
use regex::Regex;

/// A controller for the /static route.
pub struct ResourceController {}

impl ResourceController {
  /// Obtains the HTTP ETag of the file metadata.
  ///
  /// # Arguments
  /// `file`: The file data.
  fn get_file_etag(file: &[u8]) -> String {
    let mut hasher = Hasher::new();
    hasher.update(file);
    hasher.finalize().to_string()
  }

  /// Sends the file or a cache hit response.
  ///
  /// # Arguments
  /// `req`: The request.
  /// `path`: The path of the file.
  fn send_file_or_cache(
    req: &Request<Vec<u8>>,
    path: &str,
  ) -> Response<Vec<u8>> {
    let data = match Asset::get(path) {
      Some(x) => x,
      None => return text_response(404, "404 Not Found").unwrap(),
    };

    let file_etag = ResourceController::get_file_etag(&data);
    if let Some(etag) = req.headers().get(IF_NONE_MATCH) {
      if let Ok(etag) = etag.to_str() {
        if etag == file_etag {
          return Response::builder()
            .version(Version::HTTP_11)
            .status(304)
            .body(Vec::new())
            .unwrap();
        }
      }
    }

    let file_etag_val = HeaderValue::from_str(&file_etag);
    let file_etag_val = match file_etag_val {
      Ok(v) => v,
      Err(err) => {
        eprintln!(
          "Can not convert etag {} to header value: {:?}",
          file_etag, err
        );
        return text_response(500, "500 Internal Server Error").unwrap();
      }
    };

    let cache_control = HeaderValue::from_str("max-age=3600");
    let cache_control = match cache_control {
      Ok(v) => v,
      Err(err) => {
        eprintln!("Can not convert cache control to header value: {}", err);
        return text_response(500, "500 Internal Server Error").unwrap();
      }
    };

    Response::builder()
      .version(Version::HTTP_11)
      .header(ETAG, file_etag_val)
      .header(CACHE_CONTROL, cache_control)
      .status(200)
      .body(data.to_vec())
      .unwrap()
  }
}

impl Controller for ResourceController {
  fn handle_request(&self, req: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    const PREFIX: &str = "/static";
    let path = req.uri().path();
    assert!(path.starts_with(PREFIX));

    let path = &path[PREFIX.len()..];
    let path = "app/static".to_string() + path;

    let matcher = Regex::new(
      r"^app/static(?:/[a-zA-Z0-9-_]+)*/[a-zA-Z0-9-_]+(?:\.[a-zA-Z0-9-_]+)*$",
    )
    .unwrap();
    if matcher.is_match(&path) {
      let mut response = ResourceController::send_file_or_cache(req, &path);

      let parts: Vec<&str> = path.rsplitn(2, '.').collect();
      if !parts.is_empty() && parts[0] == "css" {
        let mime = HeaderValue::from_str("text/css");
        let mime = match mime {
          Ok(v) => v,
          Err(err) => {
            eprintln!("Can not get mime type for css: {:?}", err);
            return text_response(500, "500 Internal Server Error").unwrap();
          }
        };

        response.headers_mut().insert(CONTENT_TYPE, mime);
      }

      response
    } else {
      text_response(403, "403 Forbidden").unwrap()
    }
  }
}
