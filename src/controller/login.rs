//! Provides a controller for the /login route.
use super::{text_response, Controller};
use form_urlencoded;
use http::header::{HeaderValue, CONTENT_TYPE, LOCATION};
use http::method::Method;
use http::{Request, Response, Version};
use std::borrow::Cow;

/// A controller for the /login route.
pub struct LoginController {}

impl LoginController {
  /// Handles a request with x-www-form-urlencoded data.
  ///
  /// # Arguments
  /// `req`: The request.
  fn handle_data(req: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    let _data: Vec<(Cow<str>, Cow<str>)> =
      form_urlencoded::parse(req.body()).collect();
    text_response(501, "501 Not Implemented").unwrap()
  }
}

impl Controller for LoginController {
  fn handle_request(&self, req: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    if req.method() != Method::POST {
      // TODO: need a base url here. otherwise it gets difficult behind a proxy.
      let loc = HeaderValue::from_str("/");
      let loc = match loc {
        Ok(v) => v,
        Err(err) => {
          eprintln!("Can't get HeaderValue for location: {:?}", err);
          return text_response(500, "500 Internal Server Error").unwrap();
        }
      };
      return Response::builder()
        .version(Version::HTTP_11)
        .header(LOCATION, loc)
        .status(303)
        .body(Vec::new())
        .unwrap();
    }

    if let Some(content_type) = req.headers().get(CONTENT_TYPE) {
      if let Ok(content_type) = content_type.to_str() {
        if content_type == "application/x-www-form-urlencoded" {
          return LoginController::handle_data(req);
        }
      }
    }
    text_response(400, "400 Bad Request").unwrap()
  }
}
