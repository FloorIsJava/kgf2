//! Controllers for simple pages.
use super::template::TemplateController;
use http::Request;

/// A controller that just serves a template.
pub struct SimpleController {
  /// The template path.
  tpl: String,
}

impl SimpleController {
  /// Creates a new controller.
  ///
  /// # Arguments
  /// `tpl`: The template.
  pub fn new(tpl: &str) -> Self {
    SimpleController { tpl: tpl.to_string() }
  }
}

impl TemplateController for SimpleController {
  fn get_template(&self, _req: &Request<Vec<u8>>) -> &str {
    &self.tpl
  }
}
