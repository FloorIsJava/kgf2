//! Provides templating helpers for controllers.
use super::{text_response, Controller};
use crate::util::asset::Asset;
use http::{Request, Response, Version};
use std::borrow::{Borrow, Cow};
use std::error::Error;
use std::fmt::{self, Display, Formatter};
use std::str::Utf8Error;

/// A controller that loads a template as response.
pub trait TemplateController {
  /// Obtains the path to the template.
  fn get_template(&self, req: &Request<Vec<u8>>) -> &str;
}

/// An error caused by parsing a template.
#[derive(Debug)]
enum TemplateError {
  /// An error caused by an invalid template command.
  Invalid,
  /// An error caused by invalid UTF8.
  Utf8(Utf8Error),
  /// An error caused by another error.
  Surrogate(Box<dyn Error>),
  /// An error caused by a missing template.
  NoSuchTemplate,
}

/// Modes of the template parsing machine.
enum ParseMode {
  Normal,
  Command,
}

impl<T> Controller for T
where
  T: TemplateController,
{
  fn handle_request(&self, req: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    match read_template(self.get_template(req)) {
      Some(buf) => match parse_template(&buf) {
        Ok(buf) => Response::builder()
          .version(Version::HTTP_11)
          .status(200)
          .body(buf)
          .unwrap(),
        Err(err) => {
          eprintln!(
            "Failed to parse template {}: {}",
            self.get_template(req),
            err
          );
          text_response(500, "500 Internal Server Error").unwrap()
        }
      },
      None => {
        eprintln!("Failed to read template {}", self.get_template(req));
        text_response(500, "500 Internal Server Error").unwrap()
      }
    }
  }
}

/// Parses the template.
///
/// # Arguments
/// `tpl`: The template.
fn parse_template(tpl: &[u8]) -> Result<Vec<u8>, TemplateError> {
  let mut res = Vec::new();

  let mut mode = ParseMode::Normal;
  let mut acc = Vec::new();
  for &byte in tpl {
    match mode {
      ParseMode::Normal => {
        if byte == b'{' {
          mode = ParseMode::Command;
        } else {
          res.push(byte);
        }
      }
      ParseMode::Command => {
        if byte == b'}' {
          mode = ParseMode::Normal;
          exec_template_command(&mut res, &acc)?;
          acc.clear();
        } else if byte == b'{' && acc.len() == 0 {
          mode = ParseMode::Normal;
          res.push(b'{');
        } else {
          acc.push(byte);
        }
      }
    }
  }
  Ok(res)
}

/// Executes a template command.
///
/// # Arguments
/// `tpl`: The template.
/// `cmd`: The command.
fn exec_template_command(
  tpl: &mut Vec<u8>,
  cmd: &Vec<u8>,
) -> Result<(), TemplateError> {
  let cmd: Vec<&str> = std::str::from_utf8(cmd)?.split(' ').collect();
  if cmd.len() == 0 {
    return Err(TemplateError::Invalid);
  }

  match cmd[0] {
    "include" => {
      if cmd.len() == 2 {
        match read_template(cmd[1]) {
          Some(buf) => match parse_template(&buf) {
            Ok(mut buf) => tpl.append(&mut buf),
            Err(err) => return Err(TemplateError::Surrogate(Box::new(err))),
          },
          None => return Err(TemplateError::NoSuchTemplate),
        }
      }
    }
    _ => return Err(TemplateError::Invalid),
  }
  Ok(())
}

/// Reads a template from the asset storage.
///
/// # Arguments
/// `path`: The path of the template.
fn read_template(path: &str) -> Option<Cow<'static, [u8]>> {
  Asset::get(path)
}

impl From<Utf8Error> for TemplateError {
  fn from(err: Utf8Error) -> Self {
    TemplateError::Utf8(err)
  }
}

impl Error for TemplateError {
  fn source(&self) -> Option<&(dyn Error + 'static)> {
    match self {
      TemplateError::Utf8(e) => Some(e),
      TemplateError::Surrogate(e) => Some(e.borrow()),
      _ => None,
    }
  }
}

impl Display for TemplateError {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    write!(f, "{:?}", self)
  }
}
