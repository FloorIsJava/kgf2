//! Functionality for the web frontend.
pub mod httpparse;
pub mod httpstream;
pub mod httpwriter;
pub mod webserver;
