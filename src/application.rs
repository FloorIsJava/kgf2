//! The application.
use crate::web::webserver::WebServer;
use rustyline::error::ReadlineError;
use rustyline::Editor;

/// The application.
pub struct Application {
  /// The webserver for the frontend.
  webserver: WebServer,
}

impl Application {
  /// Creates an instance of the application.
  pub fn new() -> Self {
    Application { webserver: WebServer::new() }
  }

  /// Runs the application. This blocks until the application terminates.
  pub fn run(&mut self) {
    self.webserver.run();

    let mut editor = Editor::<()>::new();
    loop {
      let line = editor.readline("KgF # ");
      match line {
        Ok(_) => {}
        Err(ReadlineError::Interrupted) | Err(ReadlineError::Eof) => break,
        Err(e) => panic!("readline error: {}", e),
      }
    }
  }
}
