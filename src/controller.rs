//! Controllers for web endpoints.
mod login;
mod resource;
mod simple;
mod template;

use http::{Request, Response, Version};
use std::collections::BTreeMap;

/// A controller handles a request.
pub trait Controller {
  /// Handles the given request.
  ///
  /// # Arguments
  /// `req`: The request.
  fn handle_request(&self, req: &Request<Vec<u8>>) -> Response<Vec<u8>>;
}

/// Creates a text response.
pub fn text_response(code: u16, response: &str) -> ResponseResult {
  Response::builder()
    .version(Version::HTTP_11)
    .status(code)
    .header("content-type", "text/html; charset=utf-8")
    .body(response.to_string().into_bytes())
}

/// Routes paths to controllers.
pub struct Router {
  /// The route map.
  routes: BTreeMap<&'static str, Box<dyn Controller>>,
}

/// Controller for a basic 404 page.
struct RouteNotFound {}

type ResponseResult = Result<Response<Vec<u8>>, http::Error>;

impl Router {
  /// Creates a router.
  pub fn new() -> Self {
    let mut res = Router { routes: BTreeMap::new() };
    res.add_route("/", simple::SimpleController::new("app/tpl/index.html"));
    res
      .add_route("/legal", simple::SimpleController::new("app/tpl/legal.html"));
    res.add_route("/login", login::LoginController {});
    res.add_route("/static", resource::ResourceController {});
    res
  }

  /// Routes the given request through the router.
  ///
  /// # Arguments
  /// `req`: The request to be routed.
  pub fn route(&self, req: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    let mut longest = "";
    for (route, _) in &self.routes {
      if req.uri().path().starts_with(route) {
        if route.len() > longest.len() {
          longest = route;
        }
      }
    }

    match self.routes.get(longest) {
      None => RouteNotFound {}.handle_request(req),
      Some(v) => v.handle_request(req),
    }
  }

  /// Adds a route to the router.
  ///
  /// # Arguments
  /// `route`: The route.
  /// `ctrl`: The controller for the route.
  fn add_route(
    &mut self,
    route: &'static str,
    ctrl: impl Controller + 'static,
  ) {
    let b: Box<dyn Controller> = Box::new(ctrl);
    self.routes.insert(route, b);
  }
}

impl Controller for RouteNotFound {
  fn handle_request(&self, _: &Request<Vec<u8>>) -> Response<Vec<u8>> {
    text_response(404, "404 Not Found").unwrap()
  }
}
