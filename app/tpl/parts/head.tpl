<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Karten gegen Flopsigkeit</title>
    <link rel="icon" href="./static/img/favicon.ico" />
    <link rel="stylesheet" href="./static/css/form.css" type="text/css" />
    <link rel="stylesheet" href="./static/css/legal.css" type="text/css" />
    <link rel="stylesheet" href="./static/css/login.css" type="text/css" />
    <link rel="stylesheet" href="./static/css/main.css" type="text/css" />
    <link rel="stylesheet" href="./static/fontawesome/css/all.min.css" type="text/css" />
  </head>
  <body>
    <div id="main-page">
      <div id="header">
        <div id="header-text">
          Karten gegen Flopsigkeit
        </div>
      </div>
      <div id="content">